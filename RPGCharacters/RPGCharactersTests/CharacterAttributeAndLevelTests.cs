using System;
using Xunit;
using RPGCharacters;

namespace RPGCharactersTests
{
    public class CharacterAttributeAndLevelTests
    {
        
        [Fact]
        public void Constructor_CreateHero_ShouldReturnLevelOne()
        {
            // Arrage
            int startLevel = 1;
            int expected = startLevel;
            // Act
            Warrior warrior = new Warrior();
            int actual = warrior.Level;
            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void LevelUp_LevelUpACharacterOneTime_ShouldReturnLevelTwo()
        {
            // Arrage
            int levelUpOncetLevel = 2;
            int expected = levelUpOncetLevel;
            Mage mage = new Mage();
            // Act
            mage.LevelUp(1);
            int actual = mage.Level;
            // Assert
            Assert.Equal(expected, actual);

        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-4)]
        public void LevelUp_LevelUpACharacterWithALevelLessThanOne_ShouldThrowArgumentException(int level)
        {
            // Arrage
            Ranger ranger = new Ranger();

            // Assert
            Assert.Throws<ArgumentException>(() => ranger.LevelUp(level));
        }

        #region Create each character
        [Fact]
        public void Constructor_CreateAWarriorWithCorrectPrimaryAttributes_StartPrimaryAttributesShouldBeAsInTheInstruction()
        {
            // Arrange
            int expectedStrength = 5;
            int expectedDexterity = 2;
            int expectedVitality = 10;
            int expectedIntelligence = 1;
            Warrior warrior = new Warrior();

            // Act
            int actualStrength = warrior.BasePrimaryAtrribute.Strenght;
            int actualdDexterity = warrior.BasePrimaryAtrribute.Dexterity;
            int actualVitality = warrior.BasePrimaryAtrribute.Vitality;
            int actualIntelligence = warrior.BasePrimaryAtrribute.Intelligence;

            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualdDexterity);
            Assert.Equal(expectedVitality, actualVitality);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void Constructor_CreateAMageWithCorrectPrimaryAttributes_StartPrimaryAttributesShouldBeAsInTheInstruction()
        {
            // Arrange
            int expectedStrength = 1;
            int expectedDexterity = 1;
            int expectedVitality = 5;
            int expectedIntelligence = 8;
            Mage mage = new Mage();

            // Act
            int actualStrength = mage.BasePrimaryAtrribute.Strenght;
            int actualdDexterity = mage.BasePrimaryAtrribute.Dexterity;
            int actualVitality = mage.BasePrimaryAtrribute.Vitality;
            int actualIntelligence = mage.BasePrimaryAtrribute.Intelligence;

            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualdDexterity);
            Assert.Equal(expectedVitality, actualVitality);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void Constructor_CreateARangerWithCorrectPrimaryAttributes_StartPrimaryAttributesShouldBeAsInTheInstruction()
        {
            // Arrange
            int expectedStrength = 1;
            int expectedDexterity = 7;
            int expectedVitality = 8;
            int expectedIntelligence = 1;
            Ranger ranger = new Ranger();

            // Act
            int actualStrength = ranger.BasePrimaryAtrribute.Strenght;
            int actualdDexterity = ranger.BasePrimaryAtrribute.Dexterity;
            int actualVitality = ranger.BasePrimaryAtrribute.Vitality;
            int actualIntelligence = ranger.BasePrimaryAtrribute.Intelligence;

            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualdDexterity);
            Assert.Equal(expectedVitality, actualVitality);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void Constructor_CreateARougeWithCorrectPrimaryAttributes_StartPrimaryAttributesShouldBeAsInTheInstruction()
        {
            // Arrange
            int expectedStrength = 2;
            int expectedDexterity = 6;
            int expectedVitality = 8;
            int expectedIntelligence = 1;
            Rouge rouge = new Rouge();

            // Act
            int actualStrength = rouge.BasePrimaryAtrribute.Strenght;
            int actualdDexterity = rouge.BasePrimaryAtrribute.Dexterity;
            int actualVitality = rouge.BasePrimaryAtrribute.Vitality;
            int actualIntelligence = rouge.BasePrimaryAtrribute.Intelligence;

            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualdDexterity);
            Assert.Equal(expectedVitality, actualVitality);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }
        #endregion

        #region Attributes increases when level up for each character
        [Fact]
        public void LevelUp_LevelUpAWarriorPrimaryAttributesIncreaseCorrectly_PrimaryAttributesShouldIncreaseAsInTheInstruction()
        {
            // Arrange
            int expectedStrength = 8;
            int expectedDexterity = 4;
            int expectedVitality = 15;
            int expectedIntelligence = 2;
            Warrior warrior = new Warrior();

            // Act
            warrior.LevelUp(1);
            int actualStrength = warrior.BasePrimaryAtrribute.Strenght;
            int actualdDexterity = warrior.BasePrimaryAtrribute.Dexterity;
            int actualVitality = warrior.BasePrimaryAtrribute.Vitality;
            int actualIntelligence = warrior.BasePrimaryAtrribute.Intelligence;

            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualdDexterity);
            Assert.Equal(expectedVitality, actualVitality);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void LevelUp_LevelUpAMagerPrimaryAttributesIncreaseCorrectly_PrimaryAttributesShouldIncreaseAsInTheInstruction()
        {
            // Arrange
            int expectedStrength = 2;
            int expectedDexterity = 2;
            int expectedVitality = 8;
            int expectedIntelligence = 13;
            Mage mage = new Mage();

            // Act
            mage.LevelUp(1);
            int actualStrength = mage.BasePrimaryAtrribute.Strenght;
            int actualdDexterity = mage.BasePrimaryAtrribute.Dexterity;
            int actualVitality = mage.BasePrimaryAtrribute.Vitality;
            int actualIntelligence = mage.BasePrimaryAtrribute.Intelligence;

            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualdDexterity);
            Assert.Equal(expectedVitality, actualVitality);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void LevelUp_LevelUpARangerPrimaryAttributesIncreaseCorrectly_PrimaryAttributesShouldIncreaseAsInTheInstruction()
        {
            // Arrange
            int expectedStrength = 2;
            int expectedDexterity = 11;
            int expectedVitality = 11;
            int expectedIntelligence = 2;
            Ranger ranger = new Ranger();

            // Act
            ranger.LevelUp(1);
            int actualStrength = ranger.BasePrimaryAtrribute.Strenght;
            int actualdDexterity = ranger.BasePrimaryAtrribute.Dexterity;
            int actualVitality = ranger.BasePrimaryAtrribute.Vitality;
            int actualIntelligence = ranger.BasePrimaryAtrribute.Intelligence;

            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualdDexterity);
            Assert.Equal(expectedVitality, actualVitality);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void LevelUp_LevelUpARougePrimaryAttributesIncreaseCorrectly_PrimaryAttributesShouldIncreaseAsInTheInstruction()
        {
            // Arrange
            int expectedStrength = 3;
            int expectedDexterity = 10;
            int expectedVitality = 11;
            int expectedIntelligence = 2;
            Rouge rouge = new Rouge();

            // Act
            rouge.LevelUp(1);
            int actualStrength = rouge.BasePrimaryAtrribute.Strenght;
            int actualdDexterity = rouge.BasePrimaryAtrribute.Dexterity;
            int actualVitality = rouge.BasePrimaryAtrribute.Vitality;
            int actualIntelligence = rouge.BasePrimaryAtrribute.Intelligence;

            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualdDexterity);
            Assert.Equal(expectedVitality, actualVitality);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        #endregion

        [Fact]
        public void LevelUp_LevelUPAWarriorFromLevelOneToTwo_ShouldReturnCorrectSecondaryStatsAsStatedInTheInstruction()
        {
            // Arrange
            SecondaryAttribute expected = new SecondaryAttribute() { Health = 150, ArmorRating = 12, ElementalResistance = 2 };
            Warrior warrior = new Warrior();

            // Act 
            warrior.LevelUp(1);
            SecondaryAttribute actual = warrior.SecondaryAttribute;

            // Assert
            Assert.Equal(expected.Health, actual.Health);
            Assert.Equal(expected.ArmorRating, actual.ArmorRating);
            Assert.Equal(expected.ElementalResistance, actual.ElementalResistance);

        }
    }
}
