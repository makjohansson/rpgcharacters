﻿using Xunit;
using RPGCharacters;

namespace RPGCharactersTests
{
    public class ItemAndEquipmentTests
    {
        #region Equip
        [Fact]
        public void EquipWeapon_CreataAHeroAndEquipTheHeroWithAnAxeOfLevelTwo_ShouldThrowAnInvalidWeaponExeption()
        {
            // Arrange
            Weapon axe = new Weapon() { RequiredLevelToGet = 2, WeaponType = WeaponTypes.Axes, ItemSlot = Slots.Weapon};
            Warrior warrior = new Warrior();

            // Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(axe));

        }

        [Fact]
        public void EquipArmor_CreataAHeroAndEquipTheHeroWithAnPlateBodyArmorOfLevelTwo_ShouldThrowAnInvalidArmorExeption()
        {
            // Arrange
            Armor plate = new() { RequiredLevelToGet = 2, ItemSlot=Slots.Body, ArmorType = ArmorTypes.Plate };
            Warrior warrior = new();

            // Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(plate));

        }

        [Fact]
        public void EquipWeapon_EquipAwarriorWithABow_ShouldThrowAnInvalidWeaponExeption()
        {
            // Arrange
            Weapon bow = new() { 
                RequiredLevelToGet = 2, 
                WeaponType = WeaponTypes.Bows, 
                ItemSlot = Slots.Weapon,
                AttackSpeed = 1.3,
                Damage = 7
            };
            Warrior warrior = new();

            // Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(bow));

        }

        [Fact]
        public void EquipArmor_EquipAwarriorWithACloth_ShouldThrowAnInvalidArmorExeption()
        {
            // Arrange
            Armor cloth = new() { 
                RequiredLevelToGet = 2, 
                ItemSlot = Slots.Body, 
                ArmorType = ArmorTypes.Cloth 
            };
            Warrior warrior = new();

            // Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(cloth));

        }

        [Fact]
        public void EquipWeapon_EquipAwarriorWithASword_ShouldReturnASuccessMesage()
        {
            // Arrange
            string expected = "New weapon equipped!";
            Weapon sword = new() { 
                RequiredLevelToGet = 1,
                WeaponType=WeaponTypes.Swords, 
                ItemSlot = Slots.Weapon,
                AttackSpeed = 2.3,
                Damage = 5
            };
            Warrior warrior = new();

            // Act
            string actual = warrior.EquipWeapon(sword);

            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void EquipArmor_EquipAwarriorWithAPlate_ShouldReturnASuccessMesage()
        {
            // Arrange
            string expected = "New armor equipped!";
            Armor plate = new() { 
                RequiredLevelToGet = 1, 
                ArmorType = ArmorTypes.Plate, 
                ItemSlot = Slots.Legs
            };
            Warrior warrior = new();

            // Act
            string actual = warrior.EquipArmor(plate);

            // Assert
            Assert.Equal(expected, actual);

        }
        #endregion

        #region Calculate
        [Fact]
        public void GetHeroDPS_WarriorAtLevelOneGetDPSNoWeaponEquiped_ShouldReturnExpectedDPSAsInInstructions()
        {
            // Arrange
            double expected =  1 * (1 + (5 / 100));
            Warrior warrior = new();

            // Act
            double actual = warrior.GetHeroDPS();

            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void GetHeroDPS_WarriorAtLevelOneGetDPSWeaponEquiped_ShouldReturnExpectedDPSAsInInstructions()
        {
            // Arrange
            double expected = (7 * 1.1) * (1 + (5 / 100));
            Warrior warrior = new();
            Weapon axe = new()
            {
                AttackSpeed = 1.1,
                Damage = 7,
                WeaponType = WeaponTypes.Axes,
                ItemSlot = Slots.Weapon,
                RequiredLevelToGet = 1
            };
            warrior.EquipWeapon(axe);

            // Act
            double actual = warrior.GetHeroDPS();

            // Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void GetHeroDPS_WarriorAtLevelOneGetDPSWeaponAndArmorEquiped_ShouldReturnExpectedDPSAsInInstructions()
        {
            // Arrange
            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));
            Warrior warrior = new();
            Weapon axe = new()
            {
                AttackSpeed = 1.1,
                Damage = 7,
                WeaponType = WeaponTypes.Axes,
                ItemSlot = Slots.Weapon,
                RequiredLevelToGet = 1
            };
            Armor plate = new()
            {
                ArmorType = ArmorTypes.Plate,
                ItemSlot = Slots.Body,
                RequiredLevelToGet = 1
            };
            plate.PrimaryAttribute.Strenght = 1;
            warrior.EquipWeapon(axe);
            warrior.EquipArmor(plate);

            // Act
            double actual = warrior.GetHeroDPS();

            // Assert
            Assert.Equal(expected, actual);

        }
        #endregion

    }
}
