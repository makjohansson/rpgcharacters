﻿using System;

namespace RPGCharacters
{
    /// <summary>
    /// Class demo to display stats for a Warrior
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Warrior warrior = new Warrior();
            Weapon weapon = new Weapon() { WeaponType = WeaponTypes.Axes, RequiredLevelToGet = 1, Name = "Awsome axe", ItemSlot = Slots.Weapon, AttackSpeed = 1.3, Damage = 4};
            Armor armor = new Armor() { ItemSlot = Slots.Legs, ArmorType = ArmorTypes.Plate, Name = "Awsome plate", RequiredLevelToGet = 1 };
           
            warrior.EquipWeapon(weapon);
            warrior.EquipArmor(armor);

            Console.WriteLine(warrior.DisplayStats());
        }   
    }
}
