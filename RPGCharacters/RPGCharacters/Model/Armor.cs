﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    /// <summary>
    /// Class <c>Armor</c> inherent the <c>Item</c> class
    /// </summary>
    public class Armor : Item
    {
        public Armor()
        {
            PrimaryAttribute = new PrimaryAttribute();
        }
        public ArmorTypes ArmorType  { get; set; }

        public PrimaryAttribute PrimaryAttribute { get; set; }
    }
}
