﻿using System;
using System.Text;

namespace RPGCharacters
{
    /// <summary>
    /// Abstract class <c>Hero</c> handles logic common for each Character in the RPGCharacter game and provide necessary methods for sub classes 
    /// </summary>
    public abstract class Hero 
    {
        protected Weapon weapon;
        private int increaseHealth = 10;
        private int startLevel = 1;
        protected double increaseDamage = 0.01;
        protected Armor armor;

        public Hero()
        {
            Level = startLevel;
            Name = "Steve";
            BasePrimaryAtrribute = new PrimaryAttribute();
            TotalPrimaryAtrribute = new PrimaryAttribute();
            SecondaryAttribute = new SecondaryAttribute();
            HeroEquipment = new Equipment();
            SetUpHero();
            SetUpTotalPrimaryAttributes();
            SetUpSecondaryAttribute();
        }
        public string Name { get; set; }

        public int Level { get; set; }

        public double Damage { get; set; }

        public PrimaryAttribute BasePrimaryAtrribute { get; set; }

        public SecondaryAttribute SecondaryAttribute { get; set; }

        public PrimaryAttribute TotalPrimaryAtrribute { get; set; }

        public Equipment HeroEquipment { get; }

        /// <summary>
        /// Level up A Hero. Primary, secondary attributes and damage are increased accordingly level size.
        /// </summary>
        /// <param name="level">number of levels to level</param>
        /// <exception cref="ArgumentException">When trying to level up a level less than One</exception>
        public virtual void LevelUp(int level) 
        {
            if (level < 1)
                throw new ArgumentException("The level must be greater than zero");
            LevelUpHero(level);
            SetUpSecondaryAttribute();
            IncreaseHealthWithVitality();
            IncreaseDamage();
        }

        /// <summary>
        /// Equip a Hero with a Weapon if the Hero has a high enough level or the Weapon is available for the specific Hero
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>Success message if Weapon is successfully equiped</returns>
        /// <exception cref="InvalidWeaponException">Thrown if Hero Level not high enough for the Weapon or if the Weapon isn't available for the specific Hero</exception>
        /// <exception cref="EquipmentException">If Weapon is equipped on an invalid Slot</exception>
        public string EquipWeapon(Weapon weapon)
        {
            if (weapon.RequiredLevelToGet > Level)
                throw new InvalidWeaponException($"{Name} do not have the required level to equip this weapon");
            if (weapon.ItemSlot == Slots.Weapon)
            {
                this.weapon = weapon;
                if (EquipHeroWithWeapon())
                {
                    HeroEquipment.AddItem(weapon);
                    return "New weapon equipped!";
                }
                else
                    throw new InvalidWeaponException("This weapon can not be equiped");
            }
            else
                throw new EquipmentException("Not a valid slot for a weapon");

        }

        /// <summary>
        /// Equip a Hero with an Armor if the Hero has a high enough level or the Armor is available for the specific Hero
        /// </summary>
        /// <param name="armor"></param>
        /// <returns>Success message if Armor is successfully equiped</returns>
        /// <exception cref="InvalidArmorException">Thrown if Hero Level not high enough for the Armor or if the Armor isn't available for the specific Hero</exception>
        /// <exception cref="EquipmentException">If Armor is equipped on an invalid Slot</exception>
        public string EquipArmor(Armor armor)
        {
            if (armor.RequiredLevelToGet > Level)
                throw new InvalidArmorException($"{Name} do not have the required evel to equip this armor");
            if (armor.ItemSlot != Slots.Weapon)
            {
                this.armor = armor;
                if (EquipHeroWithArmor())
                {
                    HeroEquipment.AddItem(armor);
                    return "New armor equipped!";
                }
                else
                    throw new InvalidArmorException("This armor can not be equiped!");
            }
            else
                throw new InvalidArmorException("Not a valid slot for a armor");

        }
        /// <summary>
        /// Information about the Hero to display
        /// </summary>
        /// <returns>A string contaning information about the Hero</returns>
        public string DisplayStats()
        {
            StringBuilder stats = new StringBuilder();
            stats.AppendLine($"-- Character name: {Name}");
            stats.AppendLine($"-- Character level: {Level}");
            stats.AppendLine($"-- Strength: {TotalPrimaryAtrribute.Strenght}");
            stats.AppendLine($"-- Dexterity: {TotalPrimaryAtrribute.Dexterity}");
            stats.AppendLine($"-- Intelligence: {TotalPrimaryAtrribute.Intelligence}");
            stats.AppendLine($"-- Health: {SecondaryAttribute.Health}");
            stats.AppendLine($"-- Armor Rating: {SecondaryAttribute.ArmorRating}");
            stats.AppendLine($"-- Element Resistance: {SecondaryAttribute.ElementalResistance}");
            stats.AppendLine($"-- DPS: {GetHeroDPS()}");

            return stats.ToString();
        }

        /// <summary>
        /// DPS for a specific Hero
        /// </summary>
        /// <returns>The specific Heros DPS</returns>
        public abstract double GetHeroDPS();

        /// <summary>
        /// LevelUp hero accordingly to a specific heros BasePrimarAttribute
        /// </summary>
        /// <param name="level">number of levels to level</param>
        protected abstract void LevelUpHero(int level);

        /// <summary>
        /// Set up a hero with the level One Primary Attributes
        /// </summary>
        protected abstract void SetUpHero();

        /// <summary>
        /// Increase Damage accordinly for a specific Hero
        /// </summary>
        protected abstract void IncreaseDamage();

        /// <summary>
        /// Set up the Secondary Attributes, use when Create Hero and levelUp Hero
        /// </summary>
        private void SetUpSecondaryAttribute()
        {
            SecondaryAttribute.Health = TotalPrimaryAtrribute.Vitality * increaseHealth;
            SecondaryAttribute.ArmorRating = TotalPrimaryAtrribute.Strenght + TotalPrimaryAtrribute.Dexterity;
            SecondaryAttribute.ElementalResistance = TotalPrimaryAtrribute.Intelligence;
        }

        /// <summary>
        /// Implement the corecct logic for what Weapon each specific Hero is allowed to equip
        /// </summary>
        /// <returns>True if the Hero is allowed to equip the Weapon</returns>
        protected abstract bool EquipHeroWithWeapon();

        /// <summary>
        /// Implement the corecct logic for what Armor each specific Hero is allowed to equip
        /// </summary>
        /// <returns>True if the Hero is allowed to equip the Armor</returns>
        protected abstract bool EquipHeroWithArmor();

        /// <summary>
        /// Increase health for a Hero based on the heros Vitality
        /// </summary>
        private void IncreaseHealthWithVitality()
        {
            SecondaryAttribute.Health = BasePrimaryAtrribute.Vitality * increaseHealth;
        }

        /// <summary>
        /// Calculates the Total Primary Attributes for the Hero
        /// </summary>
        private void SetUpTotalPrimaryAttributes()
        {
            if (HeroEquipment.HasArmor())
                TotalPrimaryAtrribute = BasePrimaryAtrribute + armor.PrimaryAttribute;
            else
                TotalPrimaryAtrribute = BasePrimaryAtrribute;

        }
    }
}
