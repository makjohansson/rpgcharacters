﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    /// <summary>
    /// Enum <c>Slots</c> represent the four slots a Hero can equip Items.
    /// Head, Body, Legs are for armor items and Weapon for weapon items.
    /// </summary>
    public enum Slots
    {
        Head,
        Body,
        Legs,
        Weapon
    }
}
