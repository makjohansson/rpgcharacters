﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    /// <summary>
    /// Class <c>InvalidWeaponException</c> inherent the <c>Exception</c> class and override the Message
    /// </summary>
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException(string message) : base(message)
        {
        }
        public override string Message => "Invalid Weapon Exception";
    }
}
