﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class InvalidArmorException : Exception
    {
        /// <summary>
        /// Class <c>InvalidArmorException</c> inherent the <c>Exception</c> class and override the Message
        /// </summary>
        public InvalidArmorException(string message) : base(message)
        {
        }
        public override string Message => "Invalid Armor Exception";
    }
}
