﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    /// <summary>
    /// Class <c>Ranger</c> inherent the <c>Hero</c> class
    /// </summary>
    public class Ranger : Hero
    {
        public override double GetHeroDPS()
        {
            return weapon?.DPS() ?? 1 * (1 + (TotalPrimaryAtrribute.Dexterity / 100));
        }

        protected override void LevelUpHero(int level)
        {
            Level += level;
            BasePrimaryAtrribute.Strenght += 1 * level;
            BasePrimaryAtrribute.Dexterity += 4 * level;
            BasePrimaryAtrribute.Intelligence += 1 * level;
            BasePrimaryAtrribute.Vitality += 3 * level;
        }

        protected override void SetUpHero()
        {
            BasePrimaryAtrribute.Strenght = 1;
            BasePrimaryAtrribute.Dexterity = 7;
            BasePrimaryAtrribute.Intelligence = 1;
            BasePrimaryAtrribute.Vitality = 8;
        }

        protected override bool EquipHeroWithWeapon()
        {
            if (weapon.WeaponType == WeaponTypes.Bows)
                return true;
            else
                return false;
        }

        protected override bool EquipHeroWithArmor()
        {
            if (armor.ArmorType == ArmorTypes.Mail || armor.ArmorType == ArmorTypes.Leather)
                return true;
            else
                return false;
        }

        protected override void IncreaseDamage()
        {
            Damage = BasePrimaryAtrribute.Dexterity * increaseDamage;
        }
    }
}
