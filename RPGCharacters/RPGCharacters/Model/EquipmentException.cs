﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    /// <summary>
    /// Class <c>EquipmentException</c> inherent the <c>Exception</c> class and override the Message
    /// </summary>
    class EquipmentException : Exception
    {
        public EquipmentException(string message) : base(message)
        {
        }
        public override string Message => "Equipment Exception";

    }
}
