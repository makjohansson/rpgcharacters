﻿using System;
using System.Collections.Generic;

namespace RPGCharacters
{
    /// <summary>
    /// Class <c>Equipment</c> handles a Heros Items by putting them in a dictionary
    /// </summary>
    public class Equipment
    {

        
        private Dictionary<Slots, Item> equippedItems;
        public Equipment()
        {
            InitializeEquipment();
        }

        /// <summary>
        /// Create a default Equipment with null value for each slot
        /// </summary>
        private void InitializeEquipment()
        {
            equippedItems = new Dictionary<Slots, Item>();
            foreach (Slots slot in Enum.GetValues(typeof(Slots)))
            {
                equippedItems[slot] = null;
            }
        } 

        /// <summary>
        /// Equip or replace weapon
        /// </summary>
        /// <param name="weapon"> The weapon to equip</param>
        public void AddItem(Item item)
        {
            Console.WriteLine(item.ItemSlot);
            equippedItems[item.ItemSlot] = item;
        }

        /// <summary>
        /// Check I a Hero has an armor equipped 
        /// </summary>
        /// <returns>True if hero has an armor equipped</returns>
        public bool HasArmor()
        {
            if (equippedItems[Slots.Head] == null && equippedItems[Slots.Body] == null && equippedItems[Slots.Legs] == null)
                return false;
            return true;
        }
    }
}

