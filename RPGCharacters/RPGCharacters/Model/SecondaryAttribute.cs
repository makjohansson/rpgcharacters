﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class SecondaryAttribute
    {
        public int Health { get; set; }

        public int ArmorRating { get; set; }

        public int ElementalResistance { get; set; }
    }
}
