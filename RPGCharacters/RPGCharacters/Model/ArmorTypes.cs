﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    /// <summary>
    /// Enum <c>ArmorTypes</c> represent the four Armor types a Hero can equip.
    /// </summary>
    public enum ArmorTypes
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
}
