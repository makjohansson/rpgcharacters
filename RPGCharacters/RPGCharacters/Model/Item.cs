﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    /// <summary>
    /// Abstract class <c>Item</c> handles logic common for each Item in the RPGCharacter game and provide necessary methods for sub classes 
    /// </summary>
    public abstract class Item
    {
        public string Name { get; set; }

        public int RequiredLevelToGet { get; set; }

        public Slots ItemSlot { get; set; }
    }
}
