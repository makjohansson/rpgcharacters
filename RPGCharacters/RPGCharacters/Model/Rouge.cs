﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    /// <summary>
    /// Class <c>Rouge</c> inherent the <c>Hero</c> class
    /// </summary>
    public class Rouge : Hero
    {
        public override double GetHeroDPS()
        {
            return weapon?.DPS() ?? 1 * (1 + (TotalPrimaryAtrribute.Dexterity / 100));
        }

        protected override void LevelUpHero(int level)
        {
            Level += level;
            BasePrimaryAtrribute.Strenght += 1 * level;
            BasePrimaryAtrribute.Dexterity += 4 * level;
            BasePrimaryAtrribute.Intelligence += 1 * level;
            BasePrimaryAtrribute.Vitality += 3 * level;
        }

        protected override void SetUpHero()
        {
            BasePrimaryAtrribute.Strenght = 2;
            BasePrimaryAtrribute.Dexterity = 6;
            BasePrimaryAtrribute.Intelligence = 1;
            BasePrimaryAtrribute.Vitality = 8;
        }

        protected override bool EquipHeroWithWeapon()
        {
            if (weapon.WeaponType == WeaponTypes.Daggers || weapon.WeaponType == WeaponTypes.Swords)
                return true;
            else
                return false;
        }

        protected override bool EquipHeroWithArmor()
        {
            if (armor.ArmorType == ArmorTypes.Leather || armor.ArmorType == ArmorTypes.Mail)
                return true;
            else
                return false;
        }

        protected override void IncreaseDamage()
        {
            Damage = BasePrimaryAtrribute.Dexterity * increaseDamage;
        }
    }
}
