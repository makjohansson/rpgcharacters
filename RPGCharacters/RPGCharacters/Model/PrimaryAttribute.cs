﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class PrimaryAttribute
    {
        public int Strenght { get; set; }

        public int Dexterity { get; set; }

        public int Intelligence { get; set; }

        public int Vitality { get; set; }

        public static PrimaryAttribute operator +(PrimaryAttribute lhs, PrimaryAttribute rhs)
        {
            return new PrimaryAttribute()
            {
                Strenght = lhs.Strenght + rhs?.Strenght ?? 0,
                Dexterity = lhs.Dexterity + rhs?.Dexterity ?? 0,
                Vitality = lhs.Vitality + rhs?.Vitality ?? 0,
                Intelligence = lhs.Intelligence + rhs?.Intelligence ?? 0
            };
        }
    }
}
