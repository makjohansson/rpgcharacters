﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    /// <summary>
    /// Class <c>Warrior</c> inherent the <c>Hero</c> class
    /// </summary>
    public class Warrior : Hero
    {
        public override double GetHeroDPS()
        {
            return weapon?.DPS() ?? 1 * (1 + (TotalPrimaryAtrribute.Strenght / 100));
        }

        protected override void LevelUpHero(int level)
        {
            Level += level;
            BasePrimaryAtrribute.Strenght += 3 * level;
            BasePrimaryAtrribute.Dexterity += 2 * level;
            BasePrimaryAtrribute.Intelligence += 1 * level;
            BasePrimaryAtrribute.Vitality += 5 * level;
        }

        protected override void SetUpHero()
        {
            BasePrimaryAtrribute.Strenght = 5;
            BasePrimaryAtrribute.Dexterity = 2;
            BasePrimaryAtrribute.Intelligence = 1;
            BasePrimaryAtrribute.Vitality = 10;
        }
        
        protected override bool EquipHeroWithWeapon()
        {
            if (weapon.WeaponType == WeaponTypes.Axes || weapon.WeaponType == WeaponTypes.Hammers || weapon.WeaponType == WeaponTypes.Swords)
                return true;
            else
                return false;
        }

        protected override bool EquipHeroWithArmor()
        {
            if (armor.ArmorType == ArmorTypes.Mail || armor.ArmorType == ArmorTypes.Plate)
                return true;
            else
                return false;
        }

        protected override void IncreaseDamage()
        {
            Damage = BasePrimaryAtrribute.Strenght * increaseDamage;
        }
    }
}
