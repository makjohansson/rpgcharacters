﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    /// <summary>
    /// Class <c>Weapon</c> inherent the <c>Item</c> class
    /// </summary>
    public class Weapon : Item
    {
        public int Damage { get; set; }
        public double AttackSpeed { get; set; }
        public WeaponTypes WeaponType { get; set; }

        /// <summary>
        /// Calculate the DPS for a weapon
        /// </summary>
        /// <returns>The DPS</returns>
        public double DPS()
        {
            return Damage * AttackSpeed;
        }
    }
}
