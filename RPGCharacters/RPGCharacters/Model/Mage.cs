﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    /// <summary>
    /// Class <c>Mage</c> inherent the <c>Hero</c> class
    /// </summary>
    public class Mage : Hero
    {
        public override double GetHeroDPS()
        {
            return weapon?.DPS() ?? 1 * (1 + (TotalPrimaryAtrribute.Intelligence / 100));
        }

        protected override void LevelUpHero(int level)
        {
            Level += level;
            BasePrimaryAtrribute.Strenght +=1 * level;
            BasePrimaryAtrribute.Dexterity += 1 * level;
            BasePrimaryAtrribute.Intelligence += 5 * level;
            BasePrimaryAtrribute.Vitality += 3 * level;
        }

        protected override void SetUpHero()
        {
            BasePrimaryAtrribute.Strenght = 1;
            BasePrimaryAtrribute.Dexterity = 1;
            BasePrimaryAtrribute.Intelligence = 8;
            BasePrimaryAtrribute.Vitality = 5;
        }

        protected override bool EquipHeroWithWeapon()
        {
            if (weapon.WeaponType == WeaponTypes.Staffs || weapon.WeaponType == WeaponTypes.Wands)
                return true;
            else
                return false;
        }

        protected override bool EquipHeroWithArmor()
        {
            if (armor.ArmorType == ArmorTypes.Cloth)
                return true;
            else
                return false;
        }

        protected override void IncreaseDamage()
        {
            Damage = BasePrimaryAtrribute.Intelligence * increaseDamage;
        }
    }
}
